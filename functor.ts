import Container from "./container"

export interface Func<T,TResult>
{
    (item: T): TResult;
}

export class Functor<T> extends Container<T> {

    map<R>(f: Func<T, R>) : Functor<R> {
        return new Functor(f(this.getValue()))
    }

    toString() {
        return `Functor ( ${this.getValue()}  ) `
    }
}



// const ajouter_poire = x => x + "🍐" 
// const remplacer_par_du_chouchen = to_replace => x => x.replace(new RegExp(to_replace, "g"), "🍎")

// const bouteille_magique = new Functor("🍐");
// console.log(bouteille_magique)

// const poire_qui_tabasse = bouteille_magique
//                             .map(ajouter_poire)
//                             .map(ajouter_poire);

// console.log("Poire qui tabasse avant tansmutation", poire_qui_tabasse.getValue()) // "🍐🍐🍐"

// const chouchen =  poire_qui_tabasse.map(remplacer_par_du_chouchen("🍐"));
// console.log("Chouchen", chouchen.getValue());
// console.log("Poire qui tabasse après tansmutation", poire_qui_tabasse.getValue());
// console.log("Bouteille magique", bouteille_magique.getValue())

// const contenu : Functor<String> = new Functor("Bonjour à tous");

// const html = contenu
//     .map(content => content.split("").reverse().join(""))
//     .map(content => `<h1>${content}</h1>`)
//     .map(content => `<body>${content}</body>`)
//     .map(contenu => `<html>${contenu}</html>`);

// console.log(html.getValue()) // <html><body><h1>Bonjour à tous</h1></body></html>