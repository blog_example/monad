import {Func} from './functor';
import Container from './container'

export default class Monad<T> extends Container<T> {

    map<R>(f: Func<T, R>) : Monad<R> {
        return new Monad(f(this.getValue()))
    }

    flatMap<R> (f: Func<T, Monad<R>>) : Monad<R> {
        return f(this.getValue())
    } 
}