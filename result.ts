import { Func } from "./functor";
import { Option2 } from "./option";

interface ResultError<E> {
    error: E
}

interface ResultOk<T> {
    value: T
}

type IResult<T,E> = ResultOk<T> | ResultError<E>


class Result<T, E> {

    private result : IResult<T, E>;

    static Ok<U, E2>(value: U) : Result<U, E2> {
        return new Result({value})
    }

    static Err<U, E2>(error: E2) : Result<U, E2> {
        return new Result({error})
    }

    isErr() : Boolean {
        return (this.result as ResultError<E>).error !== undefined
    }

    private constructor(result: IResult<T, E>) {

        this.result = result
    }

    toString() : String {
        if (this.isErr()) {
            return `Error ( ${(this.result as ResultError<E>).error} )`
        }
        return `Ok ( ${(this.result as ResultOk<T>).value} )`
    }

    match<R>(resolve: Func<T, R>, reject: Func<E, R>) : R {
        if (this.isErr()) {
            return reject((this.result as ResultError<E>).error)
        }
        return resolve((this.result as ResultOk<T>).value)
    }

    ok() : Option2<T> {
        if(this.isErr()) {
            return Option2.None()
        }
        return Option2.Some((this.result as ResultOk<T>).value)
    }

    map<R>(f: Func<T, R>) : Result<R, E> {

        if (this.isErr()) return Result.Err((this.result as ResultError<E>).error)
        return Result.Ok(f((this.result as ResultOk<T>).value))
    }

    flatMap<R> (f: Func<T, Result<R, E>>) : Result<R, E> {
        if (this.isErr()) return Result.Err((this.result as ResultError<E>).error)
        return f((this.result as ResultOk<T>).value)
    }

}

console.log(`${Result.Err<number, String>('bad request')
    .map(x => x + 1)
    .flatMap(x => Result.Ok(x + 10))
    .ok()
    .orElse(666)
}
`)

console.log(`${Result.Ok<number, String>(31)
    .map(x => x + 1)
    .flatMap(x => Result.Ok(x + 10))
    .ok()
    .orElse(666)
}
`)