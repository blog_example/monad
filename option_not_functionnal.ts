function getFirstEven(tab: Array<number>) : number {
    for (let i of tab) {
        if (i % 2 == 0) {
            return i
        }
    }
}

const result1 = getFirstEven([1, 3, 5, 8])
console.log(result1 + 1) // 9

const result2 = getFirstEven([1, 3, 5, 9])

if(!isNaN(result2)) {
    console.log(result2 + 1) 
}