import Monad from './monad'

const ajouter_poire = x => new Monad(x + "🍐")
const log = step =>  x =>  {
    console.log(`contenu de la bouteille étape ${step}: ${x}`, )
    return x
}

const bouteille_magique =  new Monad("🍐");

const poire_qui_tabasse = bouteille_magique
                            .map(log(1))  // contenu de la bouteille étape 1: 🍐
                            .flatMap(ajouter_poire)
                            .map(log(2))  // contenu de la bouteille étape 2: 🍐🍐
                            .flatMap(ajouter_poire)
                            .map(log(3)); // contenu de la bouteille étape 3: 🍐🍐🍐