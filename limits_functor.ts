import {Functor} from './functor'

const ajouter_poire = x => new Functor(x + "🍐")
const log = step =>  x =>  {
    console.log(`contenu de la bouteille étape ${step}: ${x}`, )
    return x
}

const bouteille_magique =  new Functor("🍐");

const poire_qui_tabasse = bouteille_magique
                            .map(log(1))  // contenu de la bouteille étape 1: 🍐
                            .map(ajouter_poire)
                            .map(log(2))  // contenu de la bouteille étape 2: Functor ( 🍐🍐  )
                            .map(ajouter_poire)
                            .map(log(3)); // contenu de la bouteille étape 3: Functor ( Functor ( 🍐🍐  ) 🍐  )