#[derive(Debug)]
enum OptionVariant<T> {
    Some(T),
    None
}

impl <T: Copy>OptionVariant<T> {

    fn or_else(&self, else_value: T) -> T {
        match self {
            OptionVariant::Some(value) => *value,
            OptionVariant::None => else_value
        }
    }
}

fn get_even(tab: Vec<u8>) -> OptionVariant<u8> {
    for i in tab {
        if i % 2 == 0 {
            return OptionVariant::Some(i)
        }
    }
    OptionVariant::None
}

fn main() {
    let a = get_even(vec![1,5,9]);
    println!("{}", a.or_else(0));
}