package monad;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

public class Main {
    public static void main(String[] args) {
        
        List<Student> students = Arrays.asList(
            new Student("Marie")
                .addMark("maths", 17.0)
                .addMark("français", 10.0)
                .addMark("philosophie", 15.0)
                .addMark("physique", 16.0),
            new Student("Pierre")
                .addMark("maths", 11.2)
                .addMark("français", 15.0)
                .addMark("philosophie", 18.0)
                .addMark("physique", 11.0)
        );

        OptionalDouble averageMarks = students
            .parallelStream()
            .flatMap(student -> student.marks.parallelStream())
            .map(mark -> mark.value)
            .mapToDouble(x -> x)
            .average();

        System.out.println(averageMarks.orElse(0.0));
    }
}