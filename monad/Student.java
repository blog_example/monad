package monad;

import java.util.ArrayList;
import java.util.List;

public class Student {
    public String name;
    public List<Mark> marks;

    Student(String name) {
        this.name = name;
        this.marks = new ArrayList<>();
    }

    public Student addMark(String subject, double mark) {
        
        this.marks.add(new Mark(subject, mark));
        return this;
    }
}