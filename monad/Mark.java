package monad;

public class Mark {
    public String name;
    public double value;

    Mark(String name, double value) {
        this.name = name;
        this.value = value;
    }
}