class Container<T> {
    private readonly value : T;

    constructor(value: T) {

        this.value = value
    }

    getValue() : T {

        return this.value
    }
}

export default Container

// const bouteille_magique = new Container("🍐")
// console.log(bouteille_magique)
// const verre = bouteille_magique.getValue()
// const verre2 = bouteille_magique.getValue()
// const verre3 = bouteille_magique.getValue()
// console.log("verre 1: ", verre)
// console.log("verre 2: ", verre2)
// console.log("verre 3: ", verre3)