import { Func } from "./functor";

interface OptionSome<T> {
    value: T
}

interface OptionNone<T> {}

type IOption<T> = OptionSome<T> | OptionNone<T>

export class Option2<T> {

    private option : IOption<T>;

    static Some<U>(value: U) : Option2<U> {
        return new Option2({value})
    }

    static None<U>() : Option2<U> {
        return new Option2({})
    }

    public isOk() : Boolean {
        return (this.option as OptionSome<T>).value !== undefined
    }

    private constructor (option : IOption<T>) {
        this.option = option
    }

    map<R>(f: Func<T, R>) : Option2<R> {

        if (!this.isOk()) return Option2.None()
        return Option2.Some(f((this.option as OptionSome<T>).value))
    }

    flatMap<R> (f: Func<T, Option2<R>>) : Option2<R> {
        if (!this.isOk()) return Option2.None()
        return f((this.option as OptionSome<T>).value)
    }

    public orElse(fallback: T) : T {
        if (this.isOk()) {
            return (this.option as OptionSome<T>).value;
        } 
        return fallback
    }

    public toString() : String {
        if (this.isOk()) {
            return `Some(${(this.option as OptionSome<T>).value})`
        }
        return "None"
    }
}

// function getFirstEven(tab: Array<number>) : Option2<number> {

//     for (let i of tab) {

//        if (i % 2 == 0) {
//            return Option2.Some(i)
//        }
//     }

//     return Option2.None()
// }

// const log = step => x =>  {
//     console.log(`step ${step}: ${x}`)
//     return x
// }

// const with_even = getFirstEven([1, 3, 5, 8]);
// const without_even = getFirstEven([1, 3, 5, 9]);

// let a = Option2.Some(1)
//     .map(log(0))
//     .flatMap(x => (x % 2 == 0) ? Option2.Some(x) : Option2.None<number>())
//     .map(log(1))
//     .map(x => x + 2)
//     .map(log(2))
//     .orElse(666);

// console.log(a)

// console.log("\n..................\n")

// let b = Option2.Some(0)
//     .map(log(0))
//     .flatMap(x => (x % 2 == 0) ? Option2.Some(x) : Option2.None<number>())
//     .map(log(1))
//     .map(x => x + 2)
//     .map(log(2))
//     .orElse(666);

// console.log(b)


